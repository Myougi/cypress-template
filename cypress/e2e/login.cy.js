/// <reference types = "cypress"/>
import 'cypress-mochawesome-reporter/register';
import Login from '../pom/login'


describe('Test Suite Login', () => {
  let loginData;
  const lp = new Login();

  beforeEach(()=>{
    cy.fixture('login').then(data=>{
      loginData = data;
    });

    cy.visit('/sign-in-dev');
  });

  it('FPS-001, Login dgn valid username dan password', () => {
    lp.fillUsername(loginData[2].username);
    lp.fillPassword(loginData[2].password);
    lp.clickButtonLogin();
    cy.get('h2[class="fw-bolder"]')
      .should('have.text', 'Dashboard');
  });

  it('FPS-002, Login dgn invalid username dan password', () => {
    lp.fillUsername(loginData[1].username);
    lp.fillPassword(loginData[1].password);
    lp.clickButtonLogin();
    cy.get('#swal2-content')
      .should('have.text', 'Username atau Password yang anda masukan salah.');
  });

});