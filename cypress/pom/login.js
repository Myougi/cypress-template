class Login {
    visit(){
        cy.visit('/sign-in-dev');
    }

    fillUsername(username){
        return cy.get('#txtInputUsername').type(username);
    }

    fillPassword(password){
        return cy.get('#txtInputPassword').type(password);
    }

    clickButtonLogin(){
        return cy.get('#btnSubmitLogin').click();
    }
}

export default Login;