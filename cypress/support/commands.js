/// <reference types = "cypress"/>

// Login Page
Cypress.Commands.add('Login', (username, password)=>{
        cy.get('#txtInputUsername').type(username);
        cy.get('#txtInputPassword').type(password);
        cy.get('#btnSubmitLogin').click();
});